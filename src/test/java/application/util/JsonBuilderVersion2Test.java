package application.util;

import application.model.Article;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonBuilderVersion2Test {

    @Autowired
    private JsonBuilderVersion2 jsonBuilderVersion2;

    @Test
    public void buildJsonFromArticleListTest() {
        List<Article> articles = new ArrayList<>();
        articles.add(new Article(LocalDate.of(2018, 1, 7), "title1", "contents1"));
        articles.add(new Article(LocalDate.of(2018, 1, 8), "title2", "contents2"));
        String jsonExpected = "[{\"title\":\"title1\",\"contents\":\"contents1\"}," +
                "{\"title\":\"title2\",\"contents\":\"contents2\"}]";
        String jsonActual = jsonBuilderVersion2.articleListToJson(articles);
        Assert.assertEquals(jsonExpected, jsonActual);
    }
}
