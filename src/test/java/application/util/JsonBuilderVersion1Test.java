package application.util;

import application.model.Article;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonBuilderVersion1Test {

    @Autowired
    private JsonBuilderVersion1 jsonBuilderVersion1;

    @Test
    public void buildJsonFromArticleListTest() {
        List<Article> articles = new ArrayList<>();
        articles.add(new Article(LocalDate.of(2018, 1, 7), "title1", "contents1"));
        articles.add(new Article(LocalDate.of(2018, 1, 8), "title2", "contents2"));
        String jsonExpected = "[{\"date\":\"2018-01-07\",\"title\":\"title1\",\"contents\":\"contents1\"}," +
                "{\"date\":\"2018-01-08\",\"title\":\"title2\",\"contents\":\"contents2\"}]";
        String jsonActual = jsonBuilderVersion1.articleListToJson(articles);
        Assert.assertEquals(jsonExpected, jsonActual);
    }

    @Test
    public void buildJsonFromEmptyArticleListTest() {
        List<Article> articles = new ArrayList<>();
        String jsonExpected = "[]";
        String jsonActual = jsonBuilderVersion1.articleListToJson(articles);
        Assert.assertEquals(jsonExpected, jsonActual);
    }
}
