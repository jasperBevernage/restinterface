package application.facade;

import application.model.Article;
import application.service.ArticleService;
import application.util.JsonBuilderVersion1;
import application.util.JsonBuilderVersion2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class RestInterfaceTest {

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private ArticleService articleService;

    @MockBean
    private JsonBuilderVersion1 jsonBuilderVersion1;

    @MockBean
    private JsonBuilderVersion2 jsonBuilderVersion2;

    private MockMvc mockMvc;

    private ArrayList<Article> testList;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        testList = new ArrayList<>();
        Mockito.when(articleService.getPagedArticles(2, 3, false)).thenReturn(testList);
        Mockito.when(jsonBuilderVersion1.articleListToJson(testList)).thenReturn("{\"title\":\"testJson1\"}");
        Mockito.when(jsonBuilderVersion2.articleListToJson(testList)).thenReturn("{\"title\":\"testJson2\"}");
    }

    @Test
    public void mobileAppVersion1Test() throws Exception {
        mockMvc.perform(get("/getArticles").param("pageNumber", "2").param("pageSize", "3").header("User-Agent", "Mobile App version 1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("{\"title\":\"testJson1\"}"));
    }

    @Test
    public void mobileAppVersion2Test() throws Exception {
        mockMvc.perform(get("/getArticles").param("pageNumber", "2").param("pageSize", "3").header("User-Agent", "Mobile App version 2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("{\"title\":\"testJson2\"}"));
    }

    @Test
    public void mobileAppOtherVersionTest() throws Exception {
        mockMvc.perform(get("/getArticles").param("pageNumber", "2").param("pageSize", "3").header("User-Agent", "Mobile App version 3"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().string("{\"title\":\"testJson1\"}"));
    }

}
