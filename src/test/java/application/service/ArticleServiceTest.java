package application.service;

import application.database.Database;
import application.model.Article;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArticleServiceTest {

    @Autowired
    private ArticleService articleService;

    @MockBean
    private Database database;

    private List<Article> articles;

    @Before
    public void setup() {
        this.articles = new ArrayList<>();
        articles.add(new Article(LocalDate.of(2018, 1, 7), "title6", "contents6"));
        articles.add(new Article(LocalDate.of(2018, 1, 8), "title7", "contents7"));
        articles.add(new Article(LocalDate.of(2018, 1, 9), "title8", "contents8"));
        articles.add(new Article(LocalDate.of(2018, 1, 1), "title0", "contents0"));
        articles.add(new Article(LocalDate.of(2018, 1, 2), "title1", "contents1"));
        articles.add(new Article(LocalDate.of(2018, 1, 10), "title9", "contents9"));
        articles.add(new Article(LocalDate.of(2018, 1, 3), "title2", "contents2"));
        articles.add(new Article(LocalDate.of(2018, 1, 5), "title4", "contents4"));
        articles.add(new Article(LocalDate.of(2018, 1, 4), "title3", "contents3"));
        articles.add(new Article(LocalDate.of(2018, 1, 6), "title5", "contents5"));
        Mockito.when(database.getArticles()).thenReturn(articles);
        Mockito.when(database.getSize()).thenReturn(articles.size());
    }

    @Test
    public void pageNumber1PageSize3DescTest() {
        List<Article> articlesExpected = new ArrayList<>();
        articlesExpected.add(articles.get(0));
        articlesExpected.add(articles.get(9));
        articlesExpected.add(articles.get(7));
        List<Article> articlesActual = articleService.getPagedArticles(1, 3, false);
        compareArticleLists(articlesExpected, articlesActual);
    }

    @Test
    public void pageNumber1PageSize4AscTest() {
        List<Article> articlesExpected = new ArrayList<>();
        articlesExpected.add(articles.get(7));
        articlesExpected.add(articles.get(9));
        articlesExpected.add(articles.get(0));
        articlesExpected.add(articles.get(1));
        List<Article> articlesActual = articleService.getPagedArticles(1, 4, true);
        compareArticleLists(articlesExpected, articlesActual);
    }

    @Test
    public void onlyTwoResultsTest() {
        List<Article> articlesExpected = new ArrayList<>();
        articlesExpected.add(articles.get(4));
        articlesExpected.add(articles.get(3));
        List<Article> articlesActual = articleService.getPagedArticles(2, 4, false);
        compareArticleLists(articlesExpected, articlesActual);
    }

    @Test
    public void noResultsTest() {
        List<Article> articlesExpected = new ArrayList<>();
        List<Article> articlesActual = articleService.getPagedArticles(2, 5, true);
        compareArticleLists(articlesExpected, articlesActual);
    }

    @Test
    public void wrongPageNumberAndSizeTest() {
        List<Article> articlesExpected = new ArrayList<>();
        List<Article> articlesActual = articleService.getPagedArticles(0, 0, true);
        compareArticleLists(articlesExpected, articlesActual);
    }

    private void compareArticleLists(List<Article> articlesExpected, List<Article> articlesActual) {
        Assert.assertEquals(articlesExpected.size(), articlesActual.size());
        for (int i = 0; i < articlesExpected.size(); i++) {
            Assert.assertEquals(articlesExpected.get(i).getDate(), articlesActual.get(i).getDate());
            Assert.assertEquals(articlesExpected.get(i).getTitle(), articlesActual.get(i).getTitle());
            Assert.assertEquals(articlesExpected.get(i).getContents(), articlesActual.get(i).getContents());
        }
    }

}
