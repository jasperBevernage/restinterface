package application.database;

import application.model.Article;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Database class.
 */
@Component
public class Database {

    private List<Article> articles;

    public Database() {
        articles = new ArrayList<>();
        articles.add(new Article(LocalDate.of(2018, 1, 1), "title1", "contents1"));
        articles.add(new Article(LocalDate.of(2018, 1, 2), "title2", "contents2"));
        articles.add(new Article(LocalDate.of(2018, 1, 3), "title3", "contents3"));
        articles.add(new Article(LocalDate.of(2018, 1, 4), "title4", "contents4"));
        articles.add(new Article(LocalDate.of(2018, 1, 5), "title5", "contents5"));
        articles.add(new Article(LocalDate.of(2018, 1, 6), "title6", "contents6"));
        articles.add(new Article(LocalDate.of(2018, 1, 7), "title7", "contents7"));
        articles.add(new Article(LocalDate.of(2018, 1, 8), "title8", "contents8"));
        articles.add(new Article(LocalDate.of(2018, 1, 9), "title9", "contents9"));
        articles.add(new Article(LocalDate.of(2018, 1, 10), "title10", "contents10"));
        articles.add(new Article(LocalDate.of(2018, 1, 11), "title11", "contents11"));
        articles.add(new Article(LocalDate.of(2018, 1, 12), "title12", "contents12"));
        articles.add(new Article(LocalDate.of(2018, 1, 13), "title13", "contents13"));
        articles.add(new Article(LocalDate.of(2018, 1, 14), "title14", "contents14"));
        articles.add(new Article(LocalDate.of(2018, 1, 15), "title15", "contents15"));
        articles.add(new Article(LocalDate.of(2018, 1, 16), "title16", "contents16"));
        articles.add(new Article(LocalDate.of(2018, 1, 17), "title17", "contents17"));
        articles.add(new Article(LocalDate.of(2018, 1, 18), "title18", "contents18"));
        articles.add(new Article(LocalDate.of(2018, 1, 19), "title19", "contents19"));
        articles.add(new Article(LocalDate.of(2018, 1, 20), "title20", "contents20"));
        articles.add(new Article(LocalDate.of(2018, 1, 21), "title21", "contents21"));
        articles.add(new Article(LocalDate.of(2018, 1, 22), "title22", "contents22"));
    }

    /**
     * Get the number of articles.
     *
     * @return size.
     */
    public int getSize() {
        return this.articles.size();
    }

    /**
     * Get the articles.
     *
     * @return list of articles.
     */
    public List<Article> getArticles() {
        return this.articles;
    }
}
