package application.service;

import application.database.Database;
import application.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service class serving sorted and paged articles from the database.
 */
@Service
public class ArticleService {

    @Autowired
    private Database database;

    /**
     * Get a page of articles out of the database, sorted.
     *
     * @param pageNumber the page number, zero-based.
     * @param pageSize   the page size.
     * @param asc        sort ascending or descending.
     * @return list of articles.
     */
    public List<Article> getPagedArticles(int pageNumber, int pageSize, boolean asc) {
        if (pageNumber < 0 || pageSize < 1) {
            return new ArrayList<>();
        }
        int start = pageNumber * pageSize;
        int end = (pageNumber + 1) * pageSize;
        if (start > database.getSize() - 1) {
            return new ArrayList<>();
        } else {
            return database.getArticles().stream().sorted(asc ? Comparator.comparing(Article::getDate) : Comparator.comparing(Article::getDate).reversed())
                    .skip(start).limit(end - start).collect(Collectors.toList());
        }
    }
}
