package application.facade;

import application.service.ArticleService;
import application.util.JsonBuilderVersion1;
import application.util.JsonBuilderVersion2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Rest controller class implementing the 'getArticles' route.
 */
@RestController
public class RestInterface {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private JsonBuilderVersion1 jsonBuilderVersion1;

    @Autowired
    private JsonBuilderVersion2 jsonBuilderVersion2;

    /**
     * Method handling a get request to receive paged and sorted articles.
     *
     * @param pageNumber the page number.
     * @param pageSize   the page size.
     * @return JSON string.
     */
    @GetMapping(value = "/getArticles", produces = "application/json")
    public String getArticles(@RequestParam(value = "pageNumber") int pageNumber, @RequestParam(value = "pageSize") int pageSize, HttpServletRequest request) {
        //Assume that the mobile app sets the User-Agent property of the header of the http request to the correct version.
        if (request.getHeader("User-Agent").equals("Mobile App version 1")) {
            return jsonBuilderVersion1.articleListToJson(articleService.getPagedArticles(pageNumber, pageSize, false));
        } else if (request.getHeader("User-Agent").equals("Mobile App version 2")) {
            return jsonBuilderVersion2.articleListToJson(articleService.getPagedArticles(pageNumber, pageSize, false));
        } else {
            //To test out the rest interface in any browser.
            return jsonBuilderVersion1.articleListToJson(articleService.getPagedArticles(pageNumber, pageSize, false));
        }

    }
}
