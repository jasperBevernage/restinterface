package application.util;

import application.model.Article;

import java.util.List;

/**
 * Abstract class implementing the article list to JSON method.
 */
public abstract class JsonBuilderAbstract {

    /**
     * Build a JSON string out of a list of articles.
     *
     * @param articles list of articles.
     * @return JSON string.
     */
    public String articleListToJson(List<Article> articles) {
        if (articles.size() == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Article article : articles) {
            sb.append(articleToJson(article));
            sb.append(",");
        }
        sb.setLength(sb.length() - 1);
        sb.append("]");
        return sb.toString();
    }

    /**
     * Build a JSON string out of one article.
     *
     * @param article the article.
     * @return JSON string.
     */
    public abstract String articleToJson(Article article);
}
