package application.util;

import application.model.Article;
import org.springframework.stereotype.Component;

/**
 * This class implements the method to build a JSON string of one article for mobile app version 1.
 */
@Component
public class JsonBuilderVersion1 extends JsonBuilderAbstract {

    /**
     * {@inheritDoc}
     */
    @Override
    public String articleToJson(Article article) {
        return "{\"date\":\"" + article.getDate().toString() + "\",\"title\":\"" + article.getTitle() + "\",\"contents\":\"" + article.getContents() + "\"}";
    }

}
