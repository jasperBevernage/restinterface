package application.util;

import application.model.Article;
import org.springframework.stereotype.Component;

/**
 * This class implements the method to build a JSON string of one article for mobile app version 2.
 */
@Component
public class JsonBuilderVersion2 extends JsonBuilderAbstract {

    /**
     * {@inheritDoc}
     */
    @Override
    public String articleToJson(Article article) {
        return "{\"title\":\"" + article.getTitle() + "\",\"contents\":\"" + article.getContents() + "\"}";
    }

}
