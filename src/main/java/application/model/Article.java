package application.model;

import java.time.LocalDate;

/**
 * Article class.
 */
public class Article {

    private LocalDate date;
    private String title;
    private String contents;

    public Article(LocalDate date, String title, String contents) {
        this.date = date;
        this.title = title;
        this.contents = contents;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
